package ideafactory.workshop.spring.service;

import ideafactory.workshop.spring.domain.Task;
import ideafactory.workshop.spring.persistance.TaskDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Component
public class TaskServiceImpl implements TaskService {

    private TaskDao taskDao;

    @Autowired
    public TaskServiceImpl(TaskDao taskDao) {
        this.taskDao = taskDao;
    }

    @Override
    @Transactional
    public void addTask(Task task) {
        taskDao.addTask(task);
    }

    @Override
    @Transactional
    public List<Task> getTasks() {
        return taskDao.getTasks();
    }

    @Override
    @Transactional
    public boolean removeTask(Integer id) {
        return taskDao.removeTask(id);
    }

    @Override
    @Transactional
    public Task getTask(Integer id) {
        return taskDao.getTask(id);
    }

    @Override
    public void updateTask(Task task) {
        taskDao.updateTask(task);
    }
}
