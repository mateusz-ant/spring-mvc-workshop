package ideafactory.workshop.spring.service;


import ideafactory.workshop.spring.domain.Task;

import java.util.List;

public interface TaskService {
    public void addTask(Task task);
    public List<Task> getTasks();

    public boolean removeTask(Integer id);

    public Task getTask(Integer id);

    public void updateTask(Task task);

}
