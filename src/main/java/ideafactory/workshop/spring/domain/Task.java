package ideafactory.workshop.spring.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Task implements Comparable<Task> {

    private Integer id;

    @NotNull
    @Size(max = 255, min = 1, message = "Task name length must be between 1 and 255 characters!")
    private String name;

    private String description;

    private boolean done;

    public Task() { }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", done=" + done +
                '}';
    }

    @Override
    public int compareTo(Task o) {
        if (o == null || o.getId() == null) return -1;

        Integer id = o.getId();
        if (getId().equals(id)) return 0;
        if (getId() < id) return -1;
        else return 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != null) {
            return id.equals(task.id);
        }
        else {
            return task.id == null;
        }

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
