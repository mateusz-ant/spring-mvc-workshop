package ideafactory.workshop.spring.persistance;

import ideafactory.workshop.spring.domain.Task;

import java.util.List;

public interface TaskDao {
    public void addTask(Task task);
    public List<Task> getTasks();

    public boolean removeTask(Integer id);

    public Task getTask(Integer id);

    public void updateTask(Task task);

}
