package ideafactory.workshop.spring.persistance;

import ideafactory.workshop.spring.domain.Task;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Repository
public class TaskDaoImpl implements TaskDao {

    private static List<Task> TASKS;
    private static int nextTaskId = 4;

    static {
		Task t1 = new Task();
		t1.setDone(true);
		t1.setId(1);
		t1.setName("Task1");
        t1.setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit.");

		Task t2 = new Task();
		t2.setDone(false);
		t2.setId(2);
		t2.setName("Task2");
        t2.setDescription("Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.");

		Task t3 = new Task();
		t3.setDone(false);
		t3.setId(3);
		t3.setName("Task3");
        t3.setDescription("Cras vel lorem. Etiam pellentesque aliquet tellus.");

        TASKS = new ArrayList<>();
        TASKS.addAll(Arrays.asList(t1, t2, t3));
    }

	@Override
	public void addTask(Task task) {
        task.setId(nextTaskId);
        TASKS.add(task);

        nextTaskId++;
    }

	@Override
	public List<Task> getTasks() {
        Collections.sort(TASKS);
		return Collections.unmodifiableList(TASKS);
	}

	@Override
    public boolean removeTask(Integer id) {
        if (id == null) {
			throw new IllegalArgumentException("null");
		}

        Task task = getTask(id);

        return TASKS.remove(task);
    }

	@Override
	public Task getTask(Integer id) {
		if (id == null) {
			throw new IllegalArgumentException("null");
		}

		for (Task task : TASKS) {
            Integer taskId = task.getId();

            if (id.equals(taskId)) {
				return task;
			}
		}

		return null;
	}

    @Override
    public void updateTask(Task task) {
        removeTask(task.getId());

        TASKS.add(task);
    }
}
