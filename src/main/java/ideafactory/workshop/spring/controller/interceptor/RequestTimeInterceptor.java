package ideafactory.workshop.spring.controller.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @author Grzegorz Miejski
 *         on 23.03.14.
 */
public class RequestTimeInterceptor extends HandlerInterceptorAdapter {

    private Date requestStart;
    private Date requestEnd;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        requestStart = new Date();

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        requestEnd = new Date();

        long handleTimeMs = requestEnd.getTime() - requestStart.getTime();

        if (modelAndView != null) {
            modelAndView.getModelMap().addAttribute("handleTimeMs", handleTimeMs);
        }
    }

    public Date getRequestEnd() {
        return requestEnd;
    }

    public Date getRequestStart() {
        return requestStart;
    }
}
