package ideafactory.workshop.spring.controller;

import ideafactory.workshop.spring.domain.Task;
import ideafactory.workshop.spring.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private static String TASK_BINGING_RESULT_NAME = "org.springframework.validation.BindingResult.task";
    private static String TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME = "taskCreateFormFlashAttribute";
    private static String TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME = "taskEditFormFlashAttribute";

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @RequestMapping(method = GET)
    public ModelAndView listAllTasks(ModelMap modelMap) {
        modelMap.put("all_tasks", taskService.getTasks());

        return new ModelAndView("task/tasks", modelMap);
    }

    @RequestMapping(value = "/task", method = GET)
    public String showTask(@RequestParam("id") Integer taskId, ModelMap modelMap) {
        Task task = taskService.getTask(taskId);

        modelMap.addAttribute("task", task);

        return "task/details";
    }

    @RequestMapping(value = "/create", method = GET)
    public String createTask(@ModelAttribute("task") Task task, ModelMap modelMap) {

        if (modelMap.containsKey(TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME)) {
            modelMap.addAttribute(TASK_BINGING_RESULT_NAME, modelMap.get(TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME));
        }

        return "/task/create";
    }

    @RequestMapping(value = "/create", method = POST)
    public String createTaskPost(@ModelAttribute("task") @Valid Task task, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME, bindingResult);
            return "redirect:/tasks/create";
        }

        taskService.addTask(task);
        redirectAttributes.addFlashAttribute("message", "Successfully created a task!");

        return "redirect:/tasks";
    }

    @RequestMapping(value = "/delete", method = DELETE)
    @ResponseBody
    public String deleteTask(@RequestParam("id") Integer taskId) {

        boolean deleteSuccessful = taskService.removeTask(taskId);

        if (deleteSuccessful) {
            return "Successfully deleted a task!";
        } else {
            return "Could not delete task with id = " + taskId + " !";
        }
    }

    @RequestMapping(value = "/edit", method = GET)
    public String editTask(@RequestParam("id") Integer taskId, @ModelAttribute("task") Task task, ModelMap modelMap) {
        Task savedTask = taskService.getTask(taskId);

        if (modelMap.containsAttribute(TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME)) {
            modelMap.addAttribute(TASK_BINGING_RESULT_NAME, modelMap.get(TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME));

        } else {
            modelMap.addAttribute("task", savedTask);
        }
        return "/task/edit";
    }

    @RequestMapping(value = "/edit", method = POST)
    public String editTask(@ModelAttribute("task") @Valid Task task, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME, bindingResult);
            return "redirect:/tasks/edit/?id=" + task.getId();
        }

        taskService.updateTask(task);
        redirectAttributes.addFlashAttribute("message", "Task edited successfully!");

        return "redirect:/tasks";
    }

}
