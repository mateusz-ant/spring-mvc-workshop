package ideafactory.workshop.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Grzegorz Miejski
 *         on 23.03.14.
 */
@Controller
public class AuthorizationController {

    @RequestMapping(value = "/authorize", method = GET)
    public String authorize() {

        return "authorization/authorize";
    }

    @RequestMapping(value = "/authorize", method = POST)
    public String authorizePost(@RequestParam("userName") String userName, HttpServletRequest httpServletRequest, RedirectAttributes redirectAttributes) {

        if (userName != null && !userName.trim().isEmpty()) {
            httpServletRequest.getSession().setAttribute("userName", userName);

        } else {
            redirectAttributes.addFlashAttribute("message", "UserName cannot be null or empty!");
            return "redirect:/authorize";
        }

        redirectAttributes.addFlashAttribute("message", "Added user name : " + userName);

        return "redirect:/";
    }

}
