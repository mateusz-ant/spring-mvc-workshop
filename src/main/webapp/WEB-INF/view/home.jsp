<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Tasks</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css"/>
    </head>
    <body>
    <div id="content">
        <h1>Task Service!</h1>

        <h3>${message}</h3>

        <a href="${pageContext.request.contextPath}/tasks">List all tasks</a></br>
            <a href="${pageContext.request.contextPath}/tasks/create">Create new task</a></br>
            <a href="${pageContext.request.contextPath}/authorize">Authorize your task list!</a>
    </div>
    </body>
</html>