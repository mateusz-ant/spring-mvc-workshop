<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Authorization</title>
</head>
<body>
${message}
<form:form method="POST" action="${pageContext.request.contextPath}/authorize">
    <table>
        <tbody>
        <tr>
            <td>Name to remember:</td>
            <td><input name="userName" type="text"></td>
        </tr>
        <tr>
            <td><input type="submit"></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</form:form>
<a href="${pageContext.request.contextPath}/">Home page</a>
</body>
</html>
