<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Task</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css"/>
</head>
<body>
<div id="content">
    <h1><c:out value="${task.name}"/>
        <c:choose>
            <c:when test="${task.done}">(done)</c:when>
            <c:otherwise>(undone)</c:otherwise>
        </c:choose>
    </h1>
    <p>${task.description}</p>
    <a href="${pageContext.request.contextPath}/tasks">Back</a>
</div>
</body>
</html>

