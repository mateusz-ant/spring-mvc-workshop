<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Create task</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css"/>
</head>
<body>

<div id="content">
    <h1>Create task</h1>
    <form:form method="POST" action="${pageContext.request.contextPath}/tasks/create" modelAttribute="task">
        <fieldset>
            <table>
                <tr>
                    <th><form:label path="name">Name:</form:label></th>
                    <td><form:input path="name"/><br>
                        <form:errors path="name" cssClass="error"/></td>
                </tr>
                <tr>
                    <th><form:label path="description">Description:</form:label></th>
                    <td><form:input path="description"/><br>
                        <form:errors path="description" cssClass="error"/></td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input type="submit" value="Create"/>
                        <a href="${pageContext.request.contextPath}/tasks">Cancel</a>
                    </td>
                </tr>
            </table>
        </fieldset>

    </form:form>
</div>
</body>
</html>