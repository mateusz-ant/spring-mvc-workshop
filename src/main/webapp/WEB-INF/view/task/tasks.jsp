<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Tasks</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            var deleteLink = $("a:contains('Delete')");

            $(deleteLink).click(function(event) {

                $.ajax({
                    url: $(event.target).attr('href'),
                    type: 'DELETE',
                    success: function(response) {
                        var rowToDelete = $(event.target).closest("tr");
                        rowToDelete.remove();
                        $('#messages').text(response);
                    }
                });

                event.preventDefault();
            });

        });
    </script>
</head>
<body>
<div id="content">
    <div id="messages">
        ${message}
    </div>
    Session username: ${sessionScope.userName} <br />
    <h1>Tasks</h1>
    <table id="task-list">
        <c:forEach var="task" items="${all_tasks}">
            <tr>
                <td><c:out value="${task.name}"/></td>
                <td><a href="${pageContext.request.contextPath}/tasks/task?id=${task.id}">Details</a></td>
                <td><a href="${pageContext.request.contextPath}/tasks/delete?id=${task.id}">Delete</a></td>
                <td><a href="${pageContext.request.contextPath}/tasks/edit?id=${task.id}">Edit</a></td>
            </tr>
        </c:forEach>
    </table>
    <br />
    <a href="${pageContext.request.contextPath}/tasks/create">New task</a>
</div>
<div id="footer">
    This page was generated in ${handleTimeMs}ms
</div>
</body>
</html>