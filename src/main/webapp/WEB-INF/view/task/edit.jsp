<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Edit task</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/style.css"/>
</head>
<body>

<div id="content">
    <h1>Edit task</h1>
    <form:form method="POST" action="${pageContext.request.contextPath}/tasks/edit" modelAttribute="task">
        <fieldset>
            <table>
                <form:hidden path="id"/>
                <tr>
                    <th><form:label path="name">Name:</form:label></th>
                    <td><form:input path="name"/><br>
                        <form:errors path="name" cssClass="error"/></td>
                </tr>
                <tr>
                    <th><form:label path="description">Description:</form:label></th>
                    <td><form:input path="description"/> <br>
                        <form:errors path="description" cssClass="error"/></td>
                </tr>
                <tr>
                    <th><form:label path="done">Status:</form:label></th>
                    <td><form:checkbox path="done" label="Done" /></td>

                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input type="submit" value="Save"/>
                        <a href="${pageContext.request.contextPath}/tasks">Cancel</a>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form:form>
    <br/>
</div>
</body>
</html>