package ideafactory.workshop.spring.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HomeControllerTest {

	@Test
	public void shouldReturnHomeView() {
        // given
        final HomeController CONTROLLER = new HomeController();

        // when
        String viewId = CONTROLLER.showHomePage();

        // then
        assertEquals("home", viewId);
	}
}
