package ideafactory.workshop.spring.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Grzegorz Miejski
 *         on 31.03.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorizationControllerTest {

    private AuthorizationController instance = new AuthorizationController();

    private RedirectAttributes redirectAttributes;

    @Mock
    HttpServletRequest httpServletRequest;
    @Mock
    HttpSession httpSession;

    @Before
    public void setUp() throws Exception {

        redirectAttributes = new RedirectAttributesModelMap();
        when(httpServletRequest.getSession()).thenReturn(httpSession);
    }

    @Test
    public void shouldRedirectTOAuthenticationFormPage() throws Exception {

        //given
        //when
        String viewName = instance.authorize();
        //then
        assertThat(viewName).isEqualTo("authorization/authorize");
    }

    @Test
    public void shouldSetNewUserName() throws Exception {

        //given
        String userName = "Ktokolwiek";
        //when
        String viewName = instance.authorizePost(userName, httpServletRequest, redirectAttributes);
        //then
        assertThat(viewName).isEqualTo("redirect:/");
        verify(httpSession).setAttribute("userName", userName);
        assertThat(redirectAttributes.getFlashAttributes().get("message")).isNotNull();
    }

    @Test
    public void shouldNotSaveUserNameAndShouldSetRedirectAttributeWhenEmptyString() throws Exception {

        //given
        String userName = "   ";
        //when
        String viewName = instance.authorizePost(userName, httpServletRequest, redirectAttributes);
        //then
        assertThat(viewName).isEqualTo("redirect:/authorize");
        assertThat(redirectAttributes.getFlashAttributes().get("message")).isNotNull();
        verify(httpSession, never()).setAttribute("userName", userName);
    }

    @Test
    public void shouldNotSaveUserNameAndShouldSetRedirectAttributeWhenNullName() throws Exception {

        //given
        String userName = null;
        //when
        String viewName = instance.authorizePost(userName, httpServletRequest, redirectAttributes);
        //then
        assertThat(viewName).isEqualTo("redirect:/authorize");
        assertThat(redirectAttributes.getFlashAttributes().get("message")).isNotNull();
        verify(httpSession, never()).setAttribute("userName", userName);
    }
}
