package ideafactory.workshop.spring.controller;

import ideafactory.workshop.spring.domain.Task;
import ideafactory.workshop.spring.service.TaskService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author Grzegorz Miejski
 *         on 31.03.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class TaskControllerTest {

    private static String TASK_BINGING_RESULT_NAME = "org.springframework.validation.BindingResult.task";
    private static String TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME = "taskCreateFormFlashAttribute";
    private static String TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME = "taskEditFormFlashAttribute";

    @Mock
    private TaskService taskService;

    @InjectMocks
    private TaskController instance = new TaskController(taskService);

    private static List<Task> tasks;

    private ModelMap modelMap;
    private BindingResult bindingResult;
    private RedirectAttributes redirectAttributes;
    private Task task;


    @Before
    public void setUp() throws Exception {
        modelMap = new ModelMap();
        bindingResult = mock(BindingResult.class);//new MapBindingResult(new HashMap<>(), "mapa");
        task = mock(Task.class);
        redirectAttributes = new RedirectAttributesModelMap();
    }

    @BeforeClass
    public static void setTasks() {
        Task t1 = new Task();
        t1.setDone(true);
        t1.setId(1);
        t1.setName("Task1");
        t1.setDescription("Lorem ipsum dolor sit amet, consectetur adipisicing elit.");

        Task t2 = new Task();
        t2.setDone(false);
        t2.setId(2);
        t2.setName("Task2");
        t2.setDescription("Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.");

        Task t3 = new Task();
        t3.setDone(false);
        t3.setId(3);
        t3.setName("Task3");
        t3.setDescription("Cras vel lorem. Etiam pellentesque aliquet tellus.");

        tasks = new ArrayList<>();
        tasks.addAll(Arrays.asList(t1, t2, t3));
    }

    @Test
    public void shouldReturnAllTasksToView() throws Exception {

        //given
        when(taskService.getTasks()).thenReturn(tasks);

        //when
        ModelAndView modelAndView = instance.listAllTasks(modelMap);
        //then
        assertThat(modelMap.containsAttribute("all_tasks")).isTrue();
        assertThat(modelMap.get("all_tasks")).isEqualTo(tasks);
        assertThat(modelAndView.getModelMap()).isEqualTo(modelMap);
        assertThat(modelAndView.getViewName()).isEqualTo("task/tasks");

    }

    @Test
    public void shouldShowTaskDetails() throws Exception {

        //given
        int taskId = 10;
        when(taskService.getTask(taskId)).thenReturn(tasks.get(0));

        //when
        String viewName = instance.showTask(taskId, modelMap);
        //then
        assertThat(viewName).isEqualTo("task/details");
        assertThat(modelMap.containsAttribute("task")).isTrue();
        assertThat(modelMap.get("task")).isEqualTo(tasks.get(0));
    }

    @Test
    public void shouldAddBindingResultFromRedirectAttributesToModelMapWhenCreatingTask() throws Exception {

        //given
        modelMap.addAttribute(TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME, bindingResult);
        //when

        String viewName = instance.createTask(task, modelMap);

        //then
        assertThat(modelMap.containsAttribute(TASK_BINGING_RESULT_NAME)).isTrue();
        assertThat(modelMap.get(TASK_BINGING_RESULT_NAME)).isEqualTo(bindingResult);
        assertThat(viewName).isEqualTo("/task/create");
    }

    @Test
    public void shouldCreateNewTask() throws Exception {

        //given

        //when
        String viewName = instance.createTaskPost(task, bindingResult, redirectAttributes);
        //then
        verify(taskService).addTask(task);
        assertThat(viewName).isEqualTo("redirect:/tasks");
    }

    @Test
    public void shouldSetRedirectAttributesMessageWhenSuccessfullyCreatedTask() throws Exception {

        //given

        //when
        instance.createTaskPost(task, bindingResult, redirectAttributes);
        //then
        assertThat(redirectAttributes.containsAttribute("message"));
    }

    @Test
    public void shouldSetRedirectAttributeWhenValidationErrorDuringTaskCreation() throws Exception {

        //given
        when(bindingResult.hasErrors()).thenReturn(true);
        //when
        String viewName = instance.createTaskPost(task, bindingResult, redirectAttributes);
        //then
        assertThat(viewName).isEqualTo("redirect:/tasks/create");
        assertThat(redirectAttributes.containsAttribute(TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME));
        assertThat(redirectAttributes.getFlashAttributes().get(TASK_CREATE_BINDING_RESULT_FLASH_ATTRIBUTE_NAME)).isEqualTo(bindingResult);

    }

    @Test
    public void shouldDeleteTask() throws Exception {

        //given
        int taskId = tasks.get(0).getId();
        when(taskService.removeTask(taskId)).thenReturn(true);
        //when
        instance.deleteTask(taskId);
        //then
        verify(taskService).removeTask(taskId);
    }

    @Test
    public void shouldShowTaskEditWindow() throws Exception {

        //given
        Task taskInstance = tasks.get(0);
        int taskId = taskInstance.getId();
        when(taskService.getTask(taskId)).thenReturn(taskInstance);
        //when

        String viewName = instance.editTask(taskId, task, modelMap);
        //then
        assertThat(viewName).isEqualTo("/task/edit");
        assertThat(modelMap.containsAttribute("task"));
        assertThat(modelMap.get("task")).isEqualTo(taskInstance);

    }

    @Test
    public void shouldAddBindingResultFromRedirectAttributesToModelMapWhenEditingTask() throws Exception {

        //given
        Task taskInstance = tasks.get(0);
        int taskId = taskInstance.getId();
        when(taskService.getTask(taskId)).thenReturn(taskInstance);
        modelMap.addAttribute(TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME, bindingResult);

        //when
        instance.editTask(taskId, task, modelMap);
        //then
        assertThat(modelMap.containsAttribute(TASK_BINGING_RESULT_NAME));
        assertThat(modelMap.get(TASK_BINGING_RESULT_NAME)).isEqualTo(bindingResult);
    }

    @Test
    public void shouldSetRedirectAttributesWhenValidationErrorOnTaskEdit() throws Exception {

        //given
        Task taskInstance = tasks.get(0);
        int taskId = taskInstance.getId();
        when(bindingResult.hasErrors()).thenReturn(true);
        //when
        String viewName = instance.editTask(taskInstance, bindingResult, redirectAttributes);
        //then
        assertThat(redirectAttributes.containsAttribute(TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME));
        assertThat(redirectAttributes.getFlashAttributes().get(TASK_EDIT_BINDING_RESULT_FLASH_ATTRIBUTE_NAME)).isEqualTo(bindingResult);
        assertThat(viewName).isEqualTo("redirect:/tasks/edit/?id=" + taskId);
    }

    @Test
    public void shouldEditTaskSuccessfully() throws Exception {

        //given
        Task taskInstance = tasks.get(0);
        int taskId = taskInstance.getId();
        when(bindingResult.hasErrors()).thenReturn(false);
        //when
        String viewName = instance.editTask(taskInstance, bindingResult, redirectAttributes);
        //then
        assertThat(viewName).isEqualTo("redirect:/tasks");
        assertThat(redirectAttributes.getFlashAttributes().get("message")).isNotNull();
        verify(taskService).updateTask(taskInstance);
    }
}
