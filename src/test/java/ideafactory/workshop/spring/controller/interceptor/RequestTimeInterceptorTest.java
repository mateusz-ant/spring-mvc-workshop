package ideafactory.workshop.spring.controller.interceptor;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Grzegorz Miejski
 *         on 31.03.14.
 */
@RunWith(MockitoJUnitRunner.class)
public class RequestTimeInterceptorTest {

    private RequestTimeInterceptor instance = new RequestTimeInterceptor();

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    @Mock
    private Object object;

    private ModelAndView modelAndView;


    @Before
    public void setUp() throws Exception {

        modelAndView = new ModelAndView();
    }

    @Test
    public void shouldReturnTrueOnPreHandleAndSetNewRequestStartDate() throws Exception {

        //given
        //when
        boolean result = instance.preHandle(request, response, object);

        //then
        assertThat(result).isTrue();
        assertThat(instance.getRequestStart()).isNotNull();
    }

    @Test
    public void shouldAddTimeDifferenceToModelMapOnPostHandle() throws Exception {

        //given

        //when
        instance.preHandle(request, response, object);
        instance.postHandle(request, response, object, modelAndView);
        //then
        assertThat(instance.getRequestEnd()).isNotNull();
        assertThat(modelAndView.getModelMap().containsAttribute("handleTimeMs"));
    }


}
